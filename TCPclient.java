import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

public class TCPclient extends Thread{
	private Socket sock;
	private static int DATA_LEN = 4;
	private OutputStream output;
	private InputStream input;
	private boolean alive;
	
	public TCPclient (Socket sock){
		alive = true;
		this.sock = sock;
		try {
			this.sock.setSoTimeout(1000);
			output = sock.getOutputStream();
			input = sock.getInputStream();
		} catch (IOException e) {
			System.out.println("unexpected error while opening socket");
			//TODO put e in error file;
		}
	}
	
	public TCPclient(){
		this(new Socket());
	}
	
	public void connect(SocketAddress host) throws IOException{
			sock.connect(host);
		}
	}
	
	public void send(byte[] data){
		try {
			output.write(data);
		} catch (IOException e) {
			System.out.println("error sending message. RARE ERROR (probably internet issue)");
			//TODO see if what said is right haha;
		}
	}
	
	public void run(){
		while(alive){
			try{
				byte[] b = new byte[DATA_LEN];
				input.read(b);
				System.out.println(b.toString());
				//TODO USE PROTOCOL
			
		}catch (SocketTimeoutException e){
		} catch (IOException e1) {
			System.out.println("error reading data");
			close();
			//TODO print error to errof file;
			}
		}
	}
	
	public void close(){
		alive = false;
		try {sleep(2000);} catch (Exception e) {}
		try {
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
