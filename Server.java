import java.io.*;
import java.net.*;
import java.util.HashMap;


public class Server extends Thread{
	final static String HOST = "127.0.0.1";
	
	private static HashMap<SocketAddress, TCPclient> clients = new HashMap<SocketAddress, TCPclient>();
	private ServerSocket welcomeSocket;
	private boolean alive;
	
	public Server(int port) throws IOException{
		super();
		welcomeSocket = new ServerSocket(port, 10);
		welcomeSocket.setSoTimeout(1000);
		alive = true;
			
		}
	public void run(){
		while(alive){
			try {
				Socket sock = welcomeSocket.accept();
				TCPclient client = new TCPclient(sock);
				clients.put(sock.getRemoteSocketAddress(), client);
				client.start();
			} catch (SocketTimeoutException e) {
			} catch (IOException e) {
				System.out.println("problem accepting. probably internet connection.");
				//TODO print to error file
			} 	
		}
	}
	
	public void close(){
		alive = false;
		try {sleep(2000);} catch (Exception e) {}
		try {welcomeSocket.close();} catch (IOException e) {e.printStackTrace();}
		
		for(TCPclient client: clients.values()){
			client.close();
		}
		
		try {super.join();} catch (InterruptedException e) {e.printStackTrace();}
	}
}